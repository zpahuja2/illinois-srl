package edu.illinois.cs.cogcomp.srl.core;

/**
 * A list of the types of SRL that are based on the treebank. This is a
 * convenient enumeration that can be used to generalize the very similar verb
 * and nominal SRL systems.
 * 
 * @author Vivek Srikumar
 * 
 */
public enum SRLType {
	Verb, Nom
}
