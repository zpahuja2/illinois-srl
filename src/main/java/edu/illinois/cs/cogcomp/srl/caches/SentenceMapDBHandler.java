/**
 * Created by Zubin on 7/2/17.
 */

package edu.illinois.cs.cogcomp.srl.caches;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.IResetableIterator;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.utilities.SerializationHelper;
import edu.illinois.cs.cogcomp.srl.SRLProperties;
import edu.illinois.cs.cogcomp.srl.data.Dataset;

import org.mapdb.DB;
import org.mapdb.DBException;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

public class SentenceMapDBHandler implements SentenceCacheHandler {

    public static final SentenceMapDBHandler instance = new SentenceMapDBHandler(
            SRLProperties.getInstance().getSentenceDBFile());

    private DB db;

    private Logger log = org.slf4j.LoggerFactory.getLogger(SentenceMapDBHandler.class);

    private final String dbFile;

    private SentenceMapDBHandler(String dbFile) {

        this.dbFile = dbFile;

        log.info("Sentence cache {} {}found", dbFile,
                IOUtils.exists(dbFile) ? "" : "not ");

        try {
            // enabling transactions avoids cache corruption if service fails.
            // this.db = DBMaker.fileDB(dbFile).closeOnJvmShutdown().transactionEnable().make();
            this.db = DBMaker.fileDB(dbFile).closeOnJvmShutdown().make();
        }

        catch (DBException e) {
            e.printStackTrace();
            System.err.println("mapdb couldn't instantiate db using file '" + dbFile +
                    "': check error and either remove lock, repair file, or delete file.");
            throw e;
        }
    }

    /**
     * MapDB requires the database to be closed at the end of operations. This is usually handled by the
     * {@code closeOnJvmShutdown()} snippet in the initializer, but this method needs to be called if
     * multiple instances of the {@link SentenceMapDBHandler} are used.
     */
    public void close() {
        db.commit();
        db.close();
    }

    /**
     * Checks if the dataset is cached in the DB.
     *
     * @param dataset The name of the dataset (e.g. "train", "test")
     * @param dbFile The name of the MapDB file
     * @return Whether the dataset exists in the DB
     */
    public boolean isCached(Dataset dataset, String dbFile) {
        return IOUtils.exists(dbFile) && getMap(dataset.name()).size() > 0;
    }

    @Override
    public void initializeDatasets(String dbFile) {

    }

    @Override
    public void addTextAnnotation(Dataset dataset, TextAnnotation ta) {
        final ConcurrentMap<Integer, byte[]> data = getMap(dataset.name());
        try {
            data.put(ta.getTokenizedText().hashCode(), SerializationHelper.serializeTextAnnotationToBytes(ta));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        db.commit();
    }

    public void addListOfTextAnnotation(Dataset dataset, ArrayList<TextAnnotation> taList) {
        final ConcurrentMap<Integer, byte[]> data = getMap(dataset.name());
        try {
            for (TextAnnotation ta: taList) {
                data.put(ta.getTokenizedText().hashCode(), SerializationHelper.serializeTextAnnotationToBytes(ta));
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        db.commit();
    }

    @Override
    public void updateTextAnnotation(TextAnnotation ta) {
        for (String dataset : getAllDatasets()) {
            final ConcurrentMap<Integer, byte[]> data = getMap(dataset);
            try {
                data.replace(ta.getTokenizedText().hashCode(), SerializationHelper.serializeTextAnnotationToBytes(ta));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        db.commit();
    }

    @Override
    public IResetableIterator<TextAnnotation> getDataset(Dataset dataset) {
        final Collection<byte[]> list = getMap(dataset.name()).values();
        log.info("Size of dataset {} is {}", dataset.name(), list.size());
        return new IResetableIterator<TextAnnotation>() {
            Iterator<byte[]> iterator = list.iterator();

            @Override
            public void remove() {}

            @Override
            public void reset() {
                iterator = list.iterator();
            }

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public TextAnnotation next() {
                byte[] bytes = iterator.next();
                return SerializationHelper.deserializeTextAnnotationFromBytes(bytes);
            }
        };
    }

    /**
     * checks whether ta with corresponding TEXT is in database -- not whether the same
     *    annotations are present
     * @param ta
     * @return
     */
    @Override
    public boolean contains(TextAnnotation ta) {
        boolean isContained = false;
        for (String dataset : getAllDatasets()) {
            final ConcurrentMap<Integer, byte[]> data = getMap(dataset);
            isContained |= data.containsKey(ta.getTokenizedText().hashCode());
        }
        return isContained;
    }

    public void logDatasetSizes() {
        String logStr = "Size of Datasets:\n";
        for (String dataset : getAllDatasets()) {
            logStr += "\t" + dataset + " :\t" + getMap(dataset).values().size();
        }
        log.info(logStr);
    }


    @Override
    public void removeTextAnnotation(TextAnnotation ta) {
        for (String dataset : getAllDatasets()) {
            final ConcurrentMap<Integer, byte[]> data = getMap(dataset);
            data.remove(ta.getTokenizedText().hashCode());
        }
    }

    public TextAnnotation getTextAnnotation(TextAnnotation ta) {
        for (String dataset : getAllDatasets()) {
            final ConcurrentMap<Integer, byte[]> data = getMap(dataset);
            if(data.containsKey(ta.getTokenizedText().hashCode()))
            {
                byte[] taData = data.get(ta.getTokenizedText().hashCode());
                return SerializationHelper.deserializeTextAnnotationFromBytes(taData);
            }
        }
        return null;
    }

    private ConcurrentMap<Integer, byte[]> getMap(String dataset) {
        return db.hashMap(dataset, Serializer.INTEGER, Serializer.BYTE_ARRAY).createOrOpen();
    }

    @SuppressWarnings("ConstantConditions")
    @NotNull
    private Iterable<String> getAllDatasets() {
        //ReentrantReadWriteLock.ReadLock lock = db.getLock$mapdb().readLock();
        //lock.tryLock();
        Iterable<String> allNames = db.getAllNames();
        //lock.unlock();
        return allNames;
    }
}
