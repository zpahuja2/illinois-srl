package edu.illinois.cs.cogcomp.srl.caches;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.IResetableIterator;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.srl.SRLProperties;
import edu.illinois.cs.cogcomp.srl.data.Dataset;

/**
 * Created by Zubin on 7/2/17.
 */
public interface SentenceCacheHandler {

    void initializeDatasets(String dbFile);

    void addTextAnnotation(Dataset dataset, TextAnnotation ta);

    void updateTextAnnotation(TextAnnotation ta);

    IResetableIterator<TextAnnotation> getDataset(Dataset dataset);

    boolean contains(TextAnnotation ta);

    void removeTextAnnotation(TextAnnotation ta);
}